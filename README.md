# latency study

What: I came across [A day in the life of the
internet](https://wonderproxy.com/blog/a-day-in-the-life-of-the-internet/) and
I decided to use their dataset to study how naive great-circle calculations
compare to the problem of choosing a useful "closeness" metric.

A formulation of the problem is that we want a good heuristic for assigning a
set of locations to serve as VPN egress gateways.

In principle, we want to avoid very fine grained geolocation (it needs to ship
and maintain a geolocation database); for our needs it might be enough to use
an external service to just fetch a country code (or even a manual region
selection; in the past we've used the TZ locator as a fallback heuristic).

## question

* Given that we start with a country code (and we have a centroid for each
  country), how does the great-circle distance compare with a real-world
  latency metric?

In other worlds:

* How much does geographical distance deviates from latency distance? which are
  the countries that are more affected by choosing one metric over the other?

## files

* `sorted.csv`: this is the average RTT from each country in the dataset to
  each of the selected locations.

* `centroid_distance.csv`: this is the vector of locations for each country, sorted by
  spherical distance (closer first) to the geometrical centroid of each country.

## maps

### amsterdam

![ams map](amsterdam.png)

### paris

![paris map](paris.png)

### new york

![nyc map](newyork.png)

### miami

![miami map](miami.png)

### montreal

![montreal map](montreal.png)

### seattle

![seattle map](seattle.png)

# Conclusions and further ideas

* So far, it seems to me that the naive distance metric gets many countries wrong.
* Reserving "priority" allocations for a certain location for clients coming
  from a certain region (i.e, latam -> miami, or SE asia -> seattle) might be as much or
  more important than getting the closest gateway right.
* This dataset might be anecdotical. Internetworking is complex, and it will be
  subject to transient patterns. We should get better datasets (RIPE Atlas,
  OONI etc). We might want to ping the gateways as part of the RiseupVPN test.
* Allow clients to run diagnostics" (either ICMP pings, or TCP pings) against
  the gatways. Either store it, allow them to report on aggregates, or at the
  very least flag when menshen returns gateways in locations that are not optimal.
